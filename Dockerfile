FROM node:12

# Bundle app source
COPY . .

  # Install app dependencies
RUN npm install connect serve-static

EXPOSE 3000

# Start Node server
CMD [ "node", "landing_page.js" ]